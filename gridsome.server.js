// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = function (api) {
    /**
     * Manipulates the given excerpt
     */
    api.loadSource(async ({ addSchemaResolvers }) => {});

    api.onCreateNode((options) => {
        if (options.internal.typeName === 'CustomPage') {
            options.recordType = options.internal.typeName;
            options.sidebar = !!options.sidebar;
            return {
                ...options,
            };
        }
    });
};
