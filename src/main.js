// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue';

import 'prismjs/themes/prism.css';
import '~/assets/scss/main.scss';
import 'prismjs/plugins/line-numbers/prism-line-numbers.css';
import '@maltjoy/layout/dist/css/layout.css';

export default function (Vue, { router, head, isClient }) {
    // Ignore Web Components <joy-*>
    Vue.config.ignoredElements = [/^joy-.+/];

    // Set default layout as a global component
    Vue.component('Layout', DefaultLayout);
}
