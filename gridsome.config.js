function slugify(node) {
    var text = node.title;
    var text = text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    var folder = node.fileInfo.directory ? node.fileInfo.directory + '/' : '';
    return  folder + text;
}

const embedConfig = {
    enabledProviders: ['Youtube', 'Vimeo', 'Gist', 'Codepen', 'JSFiddle', 'Giphy'],
    Youtube: {
        template: './src/embedTemplates/Youtube.hbs',
    },
    Vimeo: {
        template: './src/embedTemplates/Vimeo.hbs',
    },
    Giphy: {
        template: './src/embedTemplates/Giphy.hbs',
    },
    JSFiddle: {
        template: './src/embedTemplates/JSFiddle.hbs',
        secureConnection: true,
    },
    Codepen: {
        template: './src/embedTemplates/Codepen.hbs',
    },
    Gist: {
        omitStylesheet: false,
    },
};

module.exports = {
    siteName: 'malt.engineering',
    siteUrl: 'https://www.malt.engineering',
    permalinks: {
        trailingSlash: false,
    },
    icon: {
        favicon: {
            src: './src/icon-76x76-1.png',
            sizes: [16, 32, 96],
        },
        touchicon: {
            src: './src/icon-76x76-1.png',
            sizes: [76, 152, 120, 167],
            precomposed: true,
        },
    },
    plugins: [
        {
            use: 'gridsome-source-rss',
            options: {
                feedUrl: 'https://blog.malt.engineering/feed',
                typeName: 'MediumPost',
                // Parser options, see: https://www.npmjs.com/package/rss-parser
                parser: {
                    maxRedirects: 5,
                },
            },
        },
        {
            use: 'gridsome-source-rss',
            options: {
                feedUrl: 'https://www.youtube.com/feeds/videos.xml?playlist_id=PLGG1jPu-HLW2Reolb9JqmWnUFbfIYvjhi',
                typeName: 'YoutubeTech',
                // Parser options, see: https://www.npmjs.com/package/rss-parser
                parser: {},
            },
        },
        {
            use: 'gridsome-plugin-tailwindcss',
            options: {
                tailwindConfig: './tailwind.config.js',
            },
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'CustomPage',
                baseDir: './content/pages',
                path: '**/*.md',
            },
        },
        {
            use: 'gridsome-plugin-seo',
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Events',
                baseDir: './content/events',
                path: '*.md',
            },
        },
        {
            use: '@gridsome/plugin-sitemap',
        },
        {
            use: 'gridsome-plugin-robots-txt',
        },
    ],
    templates: {
        CustomPage: (node) => {
            return `/pages/${slugify(node)}/`
        }
    },
    transformers: {
        remark: {
            plugins: [
                ['@noxify/gridsome-plugin-remark-embed', embedConfig],
                [
                    'gridsome-plugin-remark-prismjs-all',
                    {
                        noInlineHighlight: false,
                        showLineNumbers: true,
                    },
                ],
            ],
        },
    },
    chainWebpack: (config) => {
        config.resolve.alias.set('@customPageImage', '@/../content/pages');
        config.resolve.alias.set('@images', '@/assets/images');
    },
};
